import re
import os
import json
import time
from pattern.en import parse
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import  CountVectorizer, TfidfVectorizer

class TextClassifier():
    
    def __init__(self):
        self.training_raw_file_path = "./LabelledData.txt"
        self.valid_tokens           = ["who", "what", "when"]
        self.valid_tags             = ["WDT", "WP", "WP$", "WRBs", "WRB"]
        self.valid_tags_affirmation = ["MD", "VBP", "VBZ", "VB"]
        self.vectorizer             = TfidfVectorizer( max_features = None, strip_accents = 'unicode',
                                        analyzer = "word", ngram_range=(1,1), use_idf = 1, smooth_idf = 1)
        self.train()
        
    
    """
    Rule based pre classification of query in unknown and valid class
    """
    def valid_invalid_type(self, text_to_process):
        valid = False
        
        try:
            text_to_process = re.sub(" +", " ", re.sub("[^0-9A-Za-z']", " ", text_to_process.lower().strip("\n")))
            parsed_text = parse(text_to_process).split()[0]
            
            valid_flags = [True for each_parsed in parsed_text if list(set(each_parsed)&set(self.valid_tokens)) and list(set(each_parsed)&set(self.valid_tags))]
            if valid_flags and any(valid_flags): #for wh type
                valid = True
            else: #for affirmation
                if parsed_text and parsed_text[0] and parsed_text[0][1] in self.valid_tags_affirmation:
                    valid = True
        except Exception as e:
            print "Exception occured", e

        return valid

    def getTrainData(self,):
        xdata = []
        ydata = []
        self.class_list = []
        dct = {}
        try:
            print "Reading training file.."
            with open(self.training_raw_file_path, "r") as fp:
                data = fp.readlines()
            print "Done. \n\nBuilding dataset.."
            for each_line in data:
                splitted = each_line.split(",,,")
                
                if self.valid_invalid_type(splitted[0]):
                    text = " ".join(splitted[0].strip().split(" ")[:1])
                    class_name = splitted[1].strip()
                    if class_name not in dct:
                        dct[class_name] = []
                    dct[class_name].append(text)
                
            train_threshold = min([len(val_list) for val_list in dct.values()])
            # print train_threshold
            
            for k, v in dct.items():
                self.class_list.append(k)
                for count, each in enumerate(v):
                    if count == train_threshold :
                        break
                    xdata.append(each)
                    ydata.append(k)
            print "Building Done."
        except Exception as e:
            print "Exception in Training data generation", e
        # print xdata, ydata
        return xdata, ydata
        

    def train(self):
        try:
            st = int(time.time())
            print "Training process started..."
            Xtrain, Ytrain = self.getTrainData()
            vectors_train = self.vectorizer.fit_transform(Xtrain)
            self.classifier = LogisticRegression(multi_class = "multinomial", solver = "lbfgs", max_iter=1000 ) 
            self.classifier.fit(vectors_train, Ytrain)
            print "Training done!"
            print "Time taken: ", (int(time.time())-st)% 60, " seconds"
        except Exception as e:
            print "Exception in training process", e

    
    def test(self, query):
        pred = ""
        try:
            if self.valid_invalid_type(query):
                full_query = query
                first_token = query.split(" ")[0]
                
                vectors_test_full = self.vectorizer.transform([full_query])
                pred_full = self.classifier.predict(vectors_test_full)[0]
                
                vectors_test_first = self.vectorizer.transform([first_token])
                pred_first = self.classifier.predict(vectors_test_first)[0]
                
                if pred_full != pred_first:
                    pred = pred_first
                else:
                    pred = pred_full
                
            else:
                pred = "unknown"
        except Exception as e:
            print "Exception in testing", e
        
        return pred


if __name__ == '__main__':
        obj = TextClassifier()
        while(True):
            qr = raw_input("\nEnter Question=> ")
            if qr:
                result = obj.test(qr)
                print result